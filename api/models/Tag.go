package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Tag struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Tag       string    `gorm:"size:255;not null;unique" json:"tag"`
	Author    User      `json:"author"`
	AuthorID  uint32    `gorm:"not null" json:"author_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (t *Tag) Prepare() {
	t.Tag = html.EscapeString(strings.TrimSpace(t.Tag))
	t.Author = User{}
	t.CreatedAt = time.Now()
	t.UpdatedAt = time.Now()
}

func (t *Tag) Validate() map[string]string {

	var err error

	var errorMessages = make(map[string]string)

	if t.Tag == "" {
		err = errors.New("Required tag")
		errorMessages["Required_tag"] = err.Error()
	}
	if t.AuthorID < 1 {
		err = errors.New("Required Author")
		errorMessages["Required_author"] = err.Error()
	}
	return errorMessages
}

func (t *Tag) SaveTag(db *gorm.DB) (*Tag, error) {
	var err error
	err = db.Debug().Model(&Tag{}).Create(&t).Error
	if err != nil {
		return &Tag{}, err
	}
	if t.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", t.AuthorID).Take(&t.Author).Error
		if err != nil {
			return &Tag{}, err
		}
	}
	return t, nil
}

func (t *Tag) FindAllTags(db *gorm.DB) (*[]Tag, error) {
	var err error
	tags := []Tag{}
	err = db.Debug().Model(&Tag{}).Limit(100).Order("created_at desc").Find(&tags).Error
	if err != nil {
		return &[]Tag{}, err
	}
	if len(tags) > 0 {
		for t, _ := range tags {
			err := db.Debug().Model(&User{}).Where("id = ?", tags[t].AuthorID).Take(&tags[t].Author).Error
			if err != nil {
				return &[]Tag{}, err
			}
		}
	}
	return &tags, nil
}

func (t *Tag) UpdateTag(db *gorm.DB) (*Tag, error) {

	var err error

	err = db.Debug().Model(&Tag{}).Where("id = ?", t.ID).Updates(Tag{Tag: t.Tag, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Tag{}, err
	}
	if t.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", t.AuthorID).Take(&t.Author).Error
		if err != nil {
			return &Tag{}, err
		}
	}
	return t, nil
}

func (t *Tag) DeleteTag(db *gorm.DB) (int64, error) {

	db = db.Debug().Model(&Tag{}).Where("id = ?", t.ID).Take(&Tag{}).Delete(&Tag{})
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
