package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type Share struct {
	ID           uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID       uint32    `gorm:"not null" json:"user_id"`
	PostID       uint64    `gorm:"not null" json:"post_id"`
	PostAuthorID uint32    `json:"post_author_id"`
	PostTitle    string    `json:"post_title"`
	PostContent  string    `json:"post_content"`
	Picture      string    `gorm:"size:255;null;" json:"post_picture"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (s *Share) SaveShare(db *gorm.DB) (*Share, error) {
	err := db.Debug().Model(&Share{}).Where("post_id = ? AND user_id = ?", s.PostID, s.UserID).Take(&s).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&Share{}).Create(&s).Error
			if err != nil {
				return &Share{}, err
			}
		}
	} else {
		err = errors.New("double share")
		return &Share{}, err
	}
	return s, nil
}

func (s *Share) DeleteShare(db *gorm.DB) (*Share, error) {
	var err error
	var deletedShare *Share

	err = db.Debug().Model(Share{}).Where("id = ?", s.ID).Take(&s).Error
	if err != nil {
		return &Share{}, err
	} else {
		deletedShare = s
		db = db.Debug().Model(&Share{}).Where("id = ?", s.ID).Take(&Share{}).Delete(&Share{})
		if db.Error != nil {
			fmt.Println("cant delete share: ", db.Error)
			return &Share{}, db.Error
		}
	}
	return deletedShare, nil
}

func (s *Share) GetShareInfo(db *gorm.DB, pid uint64) (*[]Share, error) {
	shares := []Share{}
	err := db.Debug().Model(&Share{}).Where("post_id = ?", pid).Find(&shares).Error
	if err != nil {
		return &[]Share{}, err
	}
	return &shares, err
}

func (s *Share) DeleteUserShares(db *gorm.DB, uid uint32) (int64, error) {
	shares := []Share{}
	db = db.Debug().Model(&Share{}).Where("user_id = ?", uid).Find(&shares).Delete(&shares)
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

func (s *Share) DeletePostShares(db *gorm.DB, pid uint64) (int64, error) {
	shares := []Share{}
	db = db.Debug().Model(&Share{}).Where("post_id = ?", pid).Find(&shares).Delete(&shares)
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
