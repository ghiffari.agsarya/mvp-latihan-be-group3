package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type UserTag struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	UserID    uint32    `gorm:"not null" json:"user_id"`
	TagID     uint64    `gorm:"not null" json:"tag_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (ut *UserTag) SaveUserTag(db *gorm.DB) (*UserTag, error) {
	err := db.Debug().Model(&UserTag{}).Where("tag_id = ? AND user_id = ?", ut.TagID, ut.UserID).Take(&ut).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = db.Debug().Model(&UserTag{}).Create(&ut).Error
			if err != nil {
				return &UserTag{}, err
			}
		}
	} else {
		err = errors.New("double Usertag")
		return &UserTag{}, err
	}
	return ut, nil
}

func (ut *UserTag) DeleteUserTag(db *gorm.DB) (*UserTag, error) {
	var err error
	var deletedUserTag *UserTag

	err = db.Debug().Model(UserTag{}).Where("id = ?", ut.ID).Take(&ut).Error
	if err != nil {
		return &UserTag{}, err
	} else {
		deletedUserTag = ut
		db = db.Debug().Model(&UserTag{}).Where("id = ?", ut.ID).Take(&UserTag{}).Delete(&UserTag{})
		if db.Error != nil {
			fmt.Println("cant delete UserTag: ", db.Error)
			return &UserTag{}, db.Error
		}
	}
	return deletedUserTag, nil
}

func (ut *UserTag) GetUserTagInfo(db *gorm.DB, pid uint64) (*[]UserTag, error) {

	tags := []UserTag{}
	err := db.Debug().Model(&UserTag{}).Where("tag_id = ?", pid).Find(&tags).Error
	if err != nil {
		return &[]UserTag{}, err
	}
	return &tags, err
}
