package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Task struct {
	ID         uint64    `gorm:"primary_key;auto_increment" json:"id"`
	TaskName   string    `gorm:"size:255;not null" json:"task"`
	TaskDesc   string    `gorm:"size:255;not null" json:"desc"`
	TaskStatus string    `gorm:"size:255;not null" json:"status"`
	Author     User      `json:"author"`
	AuthorID   uint32    `gorm:"not null" json:"author_id"`
	CreatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (ts *Task) Prepare() {
	ts.TaskName = html.EscapeString(strings.TrimSpace(ts.TaskName))
	ts.TaskDesc = html.EscapeString(strings.TrimSpace(ts.TaskDesc))
	ts.TaskStatus = html.EscapeString(strings.TrimSpace(ts.TaskStatus))
	ts.Author = User{}
	ts.CreatedAt = time.Now()
	ts.UpdatedAt = time.Now()
}

func (ts *Task) Validate() map[string]string {

	var err error

	var errorMessages = make(map[string]string)

	if ts.TaskName == "" {
		err = errors.New("Required task name")
		errorMessages["Required_taskname"] = err.Error()

	}
	if ts.TaskDesc == "" {
		err = errors.New("Required task desc")
		errorMessages["Required_taskdesc"] = err.Error()

	}
	if ts.AuthorID < 1 {
		err = errors.New("Required Author")
		errorMessages["Required_author"] = err.Error()
	}
	return errorMessages
}

func (ts *Task) AddTask(db *gorm.DB) (*Task, error) {
	var err error
	err = db.Debug().Model(&Task{}).Create(&ts).Error
	if err != nil {
		return &Task{}, err
	}
	if ts.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", ts.AuthorID).Take(&ts.Author).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return ts, nil
}

func (ts *Task) FindAllTask(db *gorm.DB) (*[]Task, error) {
	var err error
	tasks := []Task{}
	err = db.Debug().Model(&Task{}).Limit(100).Order("created_at desc").Find(&tasks).Error
	if err != nil {
		return &[]Task{}, err
	}
	if len(tasks) > 0 {
		for t, _ := range tasks {
			err := db.Debug().Model(&User{}).Where("id = ?", tasks[t].AuthorID).Take(&tasks[t].Author).Error
			if err != nil {
				return &[]Task{}, err
			}
		}
	}
	return &tasks, nil
}

func (ts *Task) FindTaskByID(db *gorm.DB, pid uint64) (*Task, error) {
	var err error
	err = db.Debug().Model(&Task{}).Where("id = ?", pid).Take(&ts).Error
	if err != nil {
		return &Task{}, err
	}
	if ts.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", ts.AuthorID).Take(&ts.Author).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return ts, nil
}

func (ts *Task) UpdateTask(db *gorm.DB) (*Task, error) {

	var err error

	err = db.Debug().Model(&Task{}).Where("id = ?", ts.ID).Updates(Task{TaskName: ts.TaskName, TaskDesc: ts.TaskDesc, TaskStatus: ts.TaskStatus, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Task{}, err
	}
	if ts.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", ts.AuthorID).Take(&ts.Author).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return ts, nil
}

func (ts *Task) DeleteTask(db *gorm.DB) (int64, error) {

	db = db.Debug().Model(&Task{}).Where("id = ?", ts.ID).Take(&Task{}).Delete(&Task{})
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
