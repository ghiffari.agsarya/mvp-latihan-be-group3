package controllers

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"servergate/api/models"
	"servergate/api/utils/formaterror"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type Credentials struct {
	ClientID     string `json:"clientid"`
	ClientSecret string `json:"secret"`
}

var cred Credentials
var conf *oauth2.Config
var state string
var store sessions.CookieStore

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

// Setup the authorization path
func Setup(redirectURL, credFile string, scopes []string, secret []byte) {
	store = sessions.NewCookieStore(secret)
	var c Credentials
	file, err := ioutil.ReadFile(credFile)
	if err != nil {
		glog.Fatalf("[Gin-OAuth] File error: %v\n", err)
	}
	json.Unmarshal(file, &c)

	conf = &oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.ClientSecret,
		RedirectURL:  redirectURL,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}
}

func Session(name string) gin.HandlerFunc {
	return sessions.Sessions(name, store)
}

func LoginHandler(ctx *gin.Context) {
	state = randToken()
	session := sessions.Default(ctx)
	session.Set("state", state)
	session.Save()
	ctx.Writer.Write([]byte("<html><title>Golang Google</title> <body> <a href='" + GetLoginURL(state) + "'><button>Login with Google!</button> </a> </body></html>"))
}

func GetLoginURL(state string) string {
	return conf.AuthCodeURL(state)
}

func (server *Server) Auth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		errList = map[string]string{}
		session := sessions.Default(ctx)
		retrievedState := session.Get("state")
		if retrievedState != ctx.Query("state") {
			ctx.AbortWithError(http.StatusUnauthorized, fmt.Errorf("Invalid session state: %s", retrievedState))
			return
		}

		tok, err := conf.Exchange(oauth2.NoContext, ctx.Query("code"))
		if err != nil {
			ctx.AbortWithError(http.StatusBadRequest, err)
			return
		}

		client := conf.Client(oauth2.NoContext, tok)
		email, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
		if err != nil {
			ctx.AbortWithError(http.StatusBadRequest, err)
			return
		}
		defer email.Body.Close()
		data, err := ioutil.ReadAll(email.Body)
		if err != nil {
			glog.Errorf("[Gin-OAuth] Could not read Body: %s", err)
			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		user := models.User{}
		err = json.Unmarshal(data, &user)
		if err != nil {
			glog.Errorf("[Gin-OAuth] Unmarshal userinfo failed: %s", err)
			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		user.Google()
		_, err = user.SaveUser(server.DB)
		if err != nil {
			formattedError := formaterror.FormatError(err.Error())
			errList = formattedError
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"status": http.StatusInternalServerError,
				"error":  errList,
			})
			return
		}
		ctx.Set("user", user)
	}
}

// var (
// 	googleOauthConfig = &oauth2.Config{
// 		RedirectURL:  "http://localhost:3000/auth",
// 		ClientID:     os.Getenv("CLIENT_ID"),
// 		ClientSecret: os.Getenv("CLIENT_SECRET"),
// 		Scopes: []string{
// 			"https://www.googleapis.com/auth/userinfo.email",
// 			"https://www.googleapis.com/auth/userinfo.profile",
// 		},
// 		Endpoint: google.Endpoint,
// 	}
// 	oauthStateString = "random"
// 	state            string
// )

// func (server *Server) HandleMain(c *gin.Context) {
// 	htmlIndex := `
// <html lang="en">
//     <head>
//         <meta charset="UTF-8" />
//         <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//         <meta
//             name="google-signin-client_id"
//             content="1081147057644-dqdhvouvllast19iote51j25bg9456p2.apps.googleusercontent.com"
//         />
//         <script
//             src="https://apis.google.com/js/platform.js"
//             async
//             defer
//         ></script>
//         <title>Document</title>
//     </head>
//     <body>
//         <a href="/login-google"><div class="g-signin2" data-onsuccess="onSignIn"></div></a>
//     </body>

//     <script>
//         function onSignIn(googleUser) {
//             var profile = googleUser.getBasicProfile();
//             console.log("ID: " + profile.getId());
//             console.log("Name: " + profile.getName());
//             console.log("Image URL: " + profile.getImageUrl());
//             console.log("Email: " + profile.getEmail());
//         }
//     </script>
// </html>`

// 	c.Writer.WriteHeader(http.StatusOK)
// 	c.Writer.Write([]byte(htmlIndex))
// 	return
// }

// func (server Server) LoginGoogle(c *gin.Context) {
// 	url := googleOauthConfig.AuthCodeURL(oauthStateString)
// 	c.Redirect(http.StatusTemporaryRedirect, url)
// }

// func (server Server) HandleLoginGoogle(c *gin.Context) {
// 	if oauthStateString != c.Query("state") {
// 		c.AbortWithError(http.StatusUnauthorized, fmt.Errorf("Invalid session state: %s", oauthStateString))
// 		return
// 	}
// 	tok, err := googleOauthConfig.Exchange(oauth2.NoContext, c.Query("code"))
// 	if err != nil {
// 		c.AbortWithError(http.StatusBadRequest, err)
// 		return
// 	}
// 	client := googleOauthConfig.Client(oauth2.NoContext, tok)
// 	email, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
// 	if err != nil {
// 		c.AbortWithError(http.StatusBadRequest, err)
// 		return
// 	}
// 	defer email.Body.Close()

// 	data, _ := ioutil.ReadAll(email.Body)
// 	log.Println("Email body: ", string(data))

// 	user := models.User{}
// 	json.Unmarshal([]byte(data), &user)
// 	if err := c.Bind(&user); err != nil {
// 		c.JSON(http.StatusUnprocessableEntity, gin.H{
// 			"status": http.StatusUnprocessableEntity,
// 			"error":  "Cannot unmarshal body",
// 		})
// 		return
// 	}
// 	user.Google()
// 	userData, err := server.SignIn(user.Email, user.Password)
// 	if err != nil {
// 		formattedError := formaterror.FormatError(err.Error())
// 		c.JSON(http.StatusUnprocessableEntity, gin.H{
// 			"status": http.StatusUnprocessableEntity,
// 			"error":  formattedError,
// 		})
// 		return
// 	}
// 	c.JSON(http.StatusOK, gin.H{
// 		"status":   http.StatusOK,
// 		"response": userData,
// 	})
// }

// func (server Server) SignInGoogle(email, password string) (map[string]interface{}, error) {
// 	var err error

// 	userData := make(map[string]interface{})

// 	user := models.User{}

// 	token, err := auth.CreateToken(user.ID)
// 	if err != nil {
// 		fmt.Println("this is the error creating the token: ", err)
// 		return nil, err
// 	}
// 	userData["token"] = token
// 	userData["id"] = user.ID
// 	userData["name"] = user.Name
// 	userData["email"] = user.Email
// 	userData["picture"] = user.Picture

// 	return userData, nil
// }
