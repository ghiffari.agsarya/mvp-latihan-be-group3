package controllers

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path"
	"servergate/api/middlewares"
	"servergate/api/models"

	"github.com/gin-gonic/gin"
)

var redirectURL, credFile string

func init() {
	bin := path.Base(os.Args[0])
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `
Usage of %s
================
`, bin)
		flag.PrintDefaults()
	}
	flag.StringVar(&redirectURL, "redirect", "http://localhost:80/auth", "URL to be redirected to after authorization.")
	flag.StringVar(&credFile, "cred-file", "creds.json", "Credential JSON file")
}

func (s *Server) initializeRoutes() {
	flag.Parse()

	scopes := []string{
		"https://www.googleapis.com/auth/userinfo.email",
		"https://www.googleapis.com/auth/userinfo.profile",
	}
	secret := []byte("secret")
	sessionName := "MVPServer3"

	Setup(redirectURL, credFile, scopes, secret)
	s.Router.Use(Session(sessionName))

	s.Router.GET("/login", LoginHandler)

	private := s.Router.Group("/auth")
	private.Use(s.Auth())
	private.GET("/", UserInfoHandler)
	private.GET("/api", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{"message": "Hello"})
	})

	v1 := s.Router.Group("/api/v1")
	{
		// Guest Route
		v1.POST("/guest/signup", s.CreateUser)
		v1.POST("/guest/login", s.Login)
		v1.GET("/guest/verify-email/:id", s.Verify)

		//Users routes
		v1.PUT("/users/:id", middlewares.TokenAuthMiddleware(), s.UpdateUser)
		v1.PUT("/picture/users/:id", middlewares.TokenAuthMiddleware(), s.UpdatePicture)
		v1.DELETE("/users/:id", middlewares.TokenAuthMiddleware(), s.DeleteUser)

		//Admin routes
		v1.GET("/admin", s.GetUsers)
		v1.GET("/admin/:id", s.GetUser)
		v1.GET("/user-accepted/admin", s.GetUserAccepted)
		v1.GET("/user-rejected/admin", s.GetUserRejected)
		v1.PUT("/admin/update-user/:id", s.Update)
		v1.PUT("/admin/reject-user/:id", s.UpdateReject)

		//Tag routes
		v1.GET("/tags", s.GetTags)
		v1.POST("/tag", middlewares.TokenAuthMiddleware(), s.CreateTag)
		v1.PUT("/tag/:id", middlewares.TokenAuthMiddleware(), s.UpdateTag)
		v1.DELETE("/tag/:id", middlewares.TokenAuthMiddleware(), s.DeleteTag)

		//Posts routes
		v1.GET("/posts", s.GetPosts)
		v1.GET("/posts/:id", s.GetPost)
		v1.POST("/post/create", middlewares.TokenAuthMiddleware(), s.CreatePost)
		v1.PUT("/post/update/:id", middlewares.TokenAuthMiddleware(), s.UpdatePost)
		v1.DELETE("/post/delete/:id", middlewares.TokenAuthMiddleware(), s.DeletePost)
		v1.GET("/post/author/:id", s.GetUserPosts)
		v1.PUT("/posts/add-picture/:id", middlewares.TokenAuthMiddleware(), s.UpdatePostPicture)

		//Group routes
		v1.GET("/groups", s.GetGroups)
		v1.GET("/groups/:id", s.GetGroup)
		v1.POST("/group/create", middlewares.TokenAuthMiddleware(), s.CreateGroup)
		v1.PUT("/group/update/:id", middlewares.TokenAuthMiddleware(), s.UpdateGroup)
		v1.DELETE("/group/delete/:id", middlewares.TokenAuthMiddleware(), s.DeleteGroup)

		//UserTag routes
		v1.GET("/usertags/:id", s.GetUserTag) //akan muncul siapa aja yg memilih interest id:
		v1.POST("/usertags/:id", middlewares.TokenAuthMiddleware(), s.InsertUserTag)
		v1.DELETE("/usertags/:id", middlewares.TokenAuthMiddleware(), s.DeleteUserTag)

		//PostTag routes
		v1.GET("/posttags/:id", s.GetPostTags)
		v1.POST("/posttags/:post/:tag", middlewares.TokenAuthMiddleware(), s.InsertPostTag)
		v1.DELETE("/posttags/:id", middlewares.TokenAuthMiddleware(), s.DeletePostTag)

		//Like route
		v1.GET("/likes/:id", s.GetLikes)
		v1.POST("/likes/:id", middlewares.TokenAuthMiddleware(), s.LikePost)
		v1.DELETE("/likes/:id", middlewares.TokenAuthMiddleware(), s.UnLikePost)

		//Comment routes
		v1.POST("/comments/:id", middlewares.TokenAuthMiddleware(), s.CreateComment)
		v1.GET("/comments/:id", s.GetComments)
		v1.PUT("/comments/:id", middlewares.TokenAuthMiddleware(), s.UpdateComment)
		v1.DELETE("/comments/:id", middlewares.TokenAuthMiddleware(), s.DeleteComment)

		//Share route
		v1.GET("/shares/:id", s.GetShares)
		v1.POST("/shares/:id", middlewares.TokenAuthMiddleware(), s.SharePost)
		v1.DELETE("/shares/:id", middlewares.TokenAuthMiddleware(), s.UnSharePost)

		//Group Member route
		v1.GET("/groupmembers/:id", s.GetGroupUsers)
		v1.POST("/groupmember/:id", middlewares.TokenAuthMiddleware(), s.CreateGroupUser)
		v1.PUT("/groupmember/update/:id", middlewares.TokenAuthMiddleware(), s.UpdateGroupUser)
		v1.DELETE("/groupmember/:id", middlewares.TokenAuthMiddleware(), s.DeleteGroupUser)

		//Task routes
		v1.GET("/tasks", s.GetTasks)
		v1.GET("/tasks/:id", s.GetTask)
		v1.POST("/task/create", middlewares.TokenAuthMiddleware(), s.CreateTask)
		v1.PUT("/task/update/:id", middlewares.TokenAuthMiddleware(), s.UpdateTask)
		v1.DELETE("/task/delete/:id", middlewares.TokenAuthMiddleware(), s.DeleteTask)

		//UserTask routes
		v1.GET("/usertask/:id", s.GetUserTask)
		v1.POST("/usertask/:id", middlewares.TokenAuthMiddleware(), s.InsertUserTask)
		v1.DELETE("/usertask/:id", middlewares.TokenAuthMiddleware(), s.DeleteUserTask)

		//GroupTask routes
		v1.GET("/grouptasks/:id", s.GetGroupTasks)
		v1.POST("/grouptasks/:group/:task", middlewares.TokenAuthMiddleware(), s.InsertGroupTask)
		v1.DELETE("/grouptasks/:id", middlewares.TokenAuthMiddleware(), s.DeleteGroupTask)
	}
}

func UserInfoHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"user": ctx.MustGet("user").(models.User)})
}
